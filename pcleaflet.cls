%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% A LaTeX-class for leaflets 
% License: GPLv3 (or later)
%
% This program is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any later
% version.
%
% This program is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
% FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
% details.
%
% You should have received a copy of the GNU General Public License along with
% this program.  If not, see <https://www.gnu.org/licenses/>.
%
% Copyright (C) 2018 Patrick Capraro (latex@havesomepi.de)
% Copyright (C) 2018 quazgar (quazgar@posteo.de)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{pcleaflet}[2018/10/05 v1.1 LaTeX-Class for Leaflets]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% OPTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\DeclareOption{9pt}{\PassOptionsToClass{fontsize=9pt,DIV=calc}{scrartcl}}
\DeclareOption{10pt}{\PassOptionsToClass{10pt}{scrartcl}}
\DeclareOption{11pt}{\PassOptionsToClass{11pt}{scrartcl}}
\DeclareOption{12pt}{\PassOptionsToClass{12pt}{scrartcl}}
% SMALLMARGIN
\newif\iftextbreite
\DeclareOption{smallmargin}{\textbreitetrue}
% RANDZUGABE
\newlength{\RZG}\setlength{\RZG}{2mm}
\DeclareOption{0mmRZG}{\setlength{\RZG}{0mm}}
\DeclareOption{1mmRZG}{\setlength{\RZG}{1mm}}
\DeclareOption{2mmRZG}{\setlength{\RZG}{2mm}}
\DeclareOption{3mmRZG}{\setlength{\RZG}{3mm}}
\DeclareOption{4mmRZG}{\setlength{\RZG}{4mm}}
\DeclareOption{5mmRZG}{\setlength{\RZG}{5mm}}
\ExecuteOptions{10pt,2mmRZG}
% Debugging options
\newif\ifborders
\DeclareOption{borders}{\borderstrue}
\ProcessOptions\relax


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CLASS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\LoadClass{scrartcl}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PACKAGES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage{calc}
\RequirePackage{ifthen}
\RequirePackage{etoolbox}
\RequirePackage{tikz}
\usetikzlibrary{positioning,calc}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MARGINS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Usually leaflets are printed on oversized paper sheets to avoid unclean
% printing at the margins. Hence for the paper size an additional margin of
% 1mm to 5mm can be chosen

\iftextbreite
	\newlength{\RandInnen}\setlength{\RandInnen}{14mm}
	\newlength{\RandAussen}\setlength{\RandAussen}{7mm}
	\newlength{\TextBreiteKurz}\setlength{\TextBreiteKurz}{83mm}
	\newlength{\TextBreiteLang}\setlength{\TextBreiteLang}{86mm}
\else
	\newlength{\RandInnen}\setlength{\RandInnen}{24mm}
	\newlength{\RandAussen}\setlength{\RandAussen}{12mm}
	\newlength{\TextBreiteKurz}\setlength{\TextBreiteKurz}{73mm}
	\newlength{\TextBreiteLang}\setlength{\TextBreiteLang}{76mm}
\fi
\RequirePackage[landscape,paperheight=297mm+\RZG+\RZG,paperwidth=210mm+\RZG+\RZG,top=\RZG,bottom=\RZG,%
left=\RandAussen+\RZG,right=\RandAussen+\RZG]{geometry}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Printing BORDERS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\ifborders
\newlength\firstfold\setlength\firstfold{97mm}
\newcommand{\@widths}{{\TextBreiteKurz, \TextBreiteLang, \TextBreiteLang}}
\fi
\newcommand\@printborder{
  \ifborders
  \ifthenelse{\equal{\value{Seite}}{4}}{
    \setlength\firstfold{100mm}
    \renewcommand{\@widths}{{\TextBreiteLang, \TextBreiteLang, \TextBreiteKurz}}
  }{}
  \begin{tikzpicture}[remember picture,overlay]
    % page margin, the region outside is the bleed region
    \draw ($(current page.north west)+(\RZG,-\RZG)$) rectangle %
    ($(current page.south east)+(-\RZG,\RZG)$) ;
    % fold lines
    \draw ($(current page.north west)+(\RZG+\firstfold,-\RZG)$) -- %
    ($(current page.south west)+(\RZG+\firstfold,\RZG)$) ;
    \draw ($(current page.north west)+(\RZG+\firstfold+100mm,-\RZG)$) -- %
    ($(current page.south west)+(\RZG+\firstfold+100mm,\RZG)$) ;
    % text boxes
    \ifthenelse{\equal{\value{Seite}}{4}}{
      \draw ($(current page.north west)+(\RZG+\RandAussen,-\RZG-\TopMargin)$) rectangle %
      ++(\TextBreiteLang,-\LeafHeight);
      \draw ($(current page.north west)+(\RZG+\RandAussen+\TextBreiteLang+\RandInnen,-\RZG-\TopMargin)$) rectangle %
      ++(\TextBreiteLang,-\LeafHeight);
      \draw ($(current page.north west)+(\RZG+\RandAussen+2*\TextBreiteLang+2*\RandInnen,-\RZG-\TopMargin)$) rectangle %
      ++(\TextBreiteKurz,-\LeafHeight);
    }{
      \draw ($(current page.north west)+(\RZG+\RandAussen,-\RZG-\TopMargin)$) rectangle %
      ++(\TextBreiteKurz,-\LeafHeight);
      \draw ($(current page.north west)+(\RZG+\RandAussen+\TextBreiteKurz+\RandInnen,-\RZG-\TopMargin)$) rectangle %
      ++(\TextBreiteLang,-\LeafHeight);
      \draw ($(current page.north west)+(\RZG+\RandAussen+\TextBreiteKurz+\TextBreiteLang+2*\RandInnen,-\RZG-\TopMargin)$) rectangle %
      ++(\TextBreiteLang,-\LeafHeight);
    }
  \end{tikzpicture}
  \fi
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NEWPAGE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Instead of plain minipages, use an environment which keeps the parskip
% settings.
\newlength{\currentparskip}
\newenvironment{leafpage}[2] {% height and width of the page
  \setlength{\currentparskip}{\parskip}% save the value
  \begin{minipage}[t][#1][t]{#2}%
    \setlength{\parskip}{\currentparskip}% restore the value
    \vspace*{-\parskip}
  }{%
  \end{minipage}%
}


% Pages are divided by the leafpage environment. For the usual 97mm+100mm+100mm
% format it is necessary to have a distinguished width for the leafpages


\newcounter{Seite}
\setcounter{Seite}{0}
\newlength{\TopMargin}\setlength{\TopMargin}{10mm}
\newlength{\BottomMargin}\setlength{\BottomMargin}{10mm}
\newlength{\LeafHeight}\setlength{\LeafHeight}{\paperheight-2\RZG-\TopMargin-\BottomMargin}
\RequirePackage{etoolbox}

%% Beginning and end of document
\AfterEndPreamble{%
  \@printborder%
  \addtocounter{Seite}{1}\begin{leafpage}{\LeafHeight}{
      \TextBreiteKurz}\vspace*{\TopMargin}}
\AtEndDocument{\end{leafpage}}


%command \NewPage substitutes the usual \newpage command
\newcommand{\NewPage}{%
	\addtocounter{Seite}{1}%
	\ifthenelse{\equal{\value{Seite}}{6}}%
  % Last sub-page -> narrower page width
		{\end{leafpage}\hspace{\RandInnen}\begin{leafpage}{\LeafHeight}{\TextBreiteKurz}\vspace*{\TopMargin}}%
		{\ifthenelse{\equal{\value{Seite}}{4}}%
      % next physical page
			{\end{leafpage}\newpage%
\@printborder\begin{leafpage}{\LeafHeight}{\TextBreiteLang}\vspace*{\TopMargin}}%
      % "normal" page
			{\end{leafpage}\hspace{\RandInnen}\begin{leafpage}{\LeafHeight}{\TextBreiteLang}\vspace*{\TopMargin}%
		}%
	}%
}%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLACEIMAGE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Switch implementation
% from https://tex.stackexchange.com/a/187710/17107
% \newcommand{\@ifequals}[3]{\ifnumcomp{#1}{=}{#2}{#3}{}}
% \newcommand{\@case}[2]{} % Dummy, so \renewcommand has something to overwrite...
% \newenvironment{@switch}[1]{\renewcommand{\@case}{\@ifequals{#1}}}{}

%% command \@topleft returns the x coordinate of the topleft corner of the page
%% Arguments:
%% 1. page number
\newcommand\@topleft[1]{%
  \ifnumcomp{#1}{=}{1}{\the\RZG}{}
  \ifnumcomp{#1}{=}{2}{\the\RZG + 97mm}{}
  \ifnumcomp{#1}{=}{3}{\the\RZG + 97mm + 100mm}{}
  \ifnumcomp{#1}{=}{4}{\the\RZG}{}
  \ifnumcomp{#1}{=}{5}{\the\RZG + 100mm}{}%
  \ifnumcomp{#1}{=}{6}{\the\RZG + 100mm + 100mm}{}%
}

%% command \PlaceImage puts an image somewhere in the background on the current
%% page
%%
%% By default the image is placed in the top-left corner.
%%
%% Arguments:
%% 1. (Optional) key-value pairs
%% - width :: 100mm
%% - height :: 297mm
%% - offset :: {0,0}
%% - angle :: 0
%% - opacity :: 1.0
%% 2. Image source
\newcommand\PlaceImage[2][]{
  \pgfkeys{
    /pcleaflet/placeimage/.is family,
    /pcleaflet/placeimage,
    width/.estore in = \PlaceImage@width,
    height/.estore in = \PlaceImage@height,
    offset/.estore in = \PlaceImage@offset,
    angle/.estore in = \PlaceImage@angle,
    opacity/.estore in = \PlaceImage@opacity,
    default/.style = {
      width = 100mm,
      height = 297mm,
      offset = {0,0},
      angle = 0,
      opacity = 1.0
    },
  }
  \pgfkeys{/pcleaflet/placeimage, default, #1}
  \begin{tikzpicture}[remember picture,overlay]
    \node[anchor=north west,inner sep=0pt,opacity=\PlaceImage@opacity] at
    ($(current page.north west) + ( \@topleft{\value{Seite}} , -\RZG ) +
    (\PlaceImage@offset) $)
    {\includegraphics[width=\PlaceImage@width,height=\PlaceImage@height,
      angle=\PlaceImage@angle]{#2}};
  \end{tikzpicture}
}


\endinput

